<?php

namespace Drupal\folder_redirect\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\redirect\Entity\Redirect;
use Drupal\redirect\RedirectRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GenerateForm.
 */
class GenerateForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal|Core|Messenger|MessengerInterface definition.
   *
   * @var |Drupal|Core|Messenger|MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal|redirect|RedirectRepository definition.
   *
   * @var |Drupal|redirect|RedirectRepository
   */
  protected $redirectRepository;

  /**
   * DefaultForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\redirect\RedirectRepository $redirect_repository
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, RedirectRepository $redirectRepository) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->redirectRepository = $redirectRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('redirect.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('folder_redirect.settings');

    $form['files_autoredirect']['title'] = [
      '#markup' => $this->t('<h2>Instructions</h2><p>Before moving the <b>source</b> folder from old location to new location, add the folder path & destination path in order to automatically generate <b>301 redirect</b> for all the files in folder. Once the redirects are generated, move the folder from the old location to the new location & verify by visiting old url if it redirects correctly to the new file location.<br><i><b>Caution:</b> If source path is changed, new set of redirects will be generated each time.</i></p>'),
    ];
    $form['folder_redirect']['scan_folder'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Folder to scan'),
      '#default_value' => $config->get('folder_redirect_scan_folder'),
      '#size'          => 60,
      '#maxlength'     => 128,
      '#description'   => $this->t('This folder must exsist & accessible under the path so all the files inside can be scanned and a redirect rule is added for each file.<br>Example: For <b>root/content</b> folder add <b>content</b>.'),
      '#required'      => TRUE,
    ];
    $form['folder_redirect']['check'] = [
      '#title'         => $this->t("Same as folder path"),
      '#type'          => 'checkbox',
      '#default_value' => !empty($config->get('folder_redirect_check')) ? $config->get('folder_redirect_check') : 1,
      '#description'   => $this->t('Uncheck if the <b>redirect source</b> path is different.'),
    ];
    $form['folder_redirect']['path_check'] = [
      '#type'   => 'container',
      '#states' => [
        "visible" => [
          "input[name='check']" => ["checked" => FALSE],
        ],
      ],
    ];
    $form['folder_redirect']['path_check']['path_from'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Redirect from path (<i>source</i>)'),
      '#default_value' => $config->get('folder_redirect_from'),
      '#size'          => 60,
      '#maxlength'     => 128,
      '#description'   => $this->t('Example: For <b>root/content</b> folder add <b>content</b>. If left blank scanned folder will be chosen as base path.'),
    ];
    $form['folder_redirect']['path_to'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Redirect to path (<i>destination</i>)'),
      '#default_value' => $config->get('folder_redirect_to'),
      '#size'          => 60,
      '#maxlength'     => 128,
      '#description'   => $this->t('Example: <b>sites/default/files/</b> for <b>root/sites/default/files</b> folder. At The End Trailing slash must be provided.'),
      '#required'      => TRUE,
    ];
    $form['folder_redirect']['exception'] = [
      '#title'         => $this->t('Exceptions'),
      '#type'          => 'textarea',
      '#description'   => $this->t('Exception rules, files or directories added in the list will be ignored. Add one entry per row.'),
      '#default_value' => !empty($config->get('folder_redirect_exceptions')) ? implode($config->get('folder_redirect_exceptions')) : implode(PHP_EOL, [
        '. ',
        '.. ',
        '.DS_Store ',
      ]),
    ];
    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Generate Redirects'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('folder_redirect.settings');
    if ($form_state->getValue('exception')) {
      $exceptions = explode(PHP_EOL, trim($form_state->getValue('exception')));
      $config->set('folder_redirect_exceptions', $exceptions)
        ->save();
    }
    // Save configurations.
    $config->set('folder_redirect_check', $form_state->getValue('check'))
      ->save();
    $config->set('folder_redirect_scan_folder', $form_state->getValue('scan_folder'))
      ->save();
    $config->set('folder_redirect_from', $form_state->getValue('path_from'))
      ->save();
    $config->set('folder_redirect_to', $form_state->getValue('path_to'))
      ->save();

    // Trigger actions.
    if (!empty($config->get('folder_redirect_scan_folder')) && !empty($config->get('folder_redirect_to'))) {
      if ($config->get('folder_redirect_check')) {
        $this->configFactory->getEditable('folder_redirect.settings.folder_redirect_from')
          ->delete();
        if ($this->_folder_redirect_trigger_import_redirects($config->get('folder_redirect_scan_folder'), $config->get('folder_redirect_to'), $config->get('folder_redirect_exceptions'))) {
          $this->messenger->addMessage($this->t("Url redirects generated successfully, <a href=\" @base-url \">Redirects List</a>", ['@base-url' => '/admin/config/search/redirect']), 'status', TRUE);
        }
        else {
          $this->messenger->addMessage($this->t('Looks like "<i> @dir </i>" doesn\'t exsist or inaccessible, please check the permission if exsists', ['@dir' => $config->get('folder_redirect_scan_folder')]), 'error', TRUE);
        }
      }
      else {
        if ($this->_folder_redirect_trigger_import_redirects($config->get('folder_redirect_from'), $config->get('folder_redirect_to'), $config->get('folder_redirect_exceptions'), $config->get('folder_redirect_scan_folder'))) {
          $this->messenger->addMessage($this->t("Url redirects generated successfully, <a href=\" @base-url \">Redirects List</a>", ['@base-url' => '/admin/config/search/redirect']), 'status', TRUE);
        }
        else {
          $this->messenger->addMessage($this->t('Looks like "<i> @dir </i>" doesn\'t exsist or inaccessible, please check the permission if exsists', ['@dir' => $config->get('folder_redirect_scan_folder')]), 'error', TRUE);
        }
      }
    }
    else {
      $this->messenger->addMessage($this->t('Invalid configurations, please try again'), 'error', TRUE);
    }
  }

  /**
   * Helper function to set important variables.
   */
  public function _folder_redirect_trigger_import_redirects($path, $path_to, $exceptions, $folder_scan = NULL) {
    $root = DRUPAL_ROOT . "/";
    $root_preg = preg_replace("/([\/]+)/", "\\/", $root);
    $path_from_preg = preg_replace("/([\/]+)/", "\\/", $path);
    if ($folder_scan) {
      $scan_folder = $root . $folder_scan;
      if (is_dir($scan_folder)) {
        $this->_folder_redirect_list_all_files($scan_folder, $path_from_preg, $path_to, $root, $root_preg, $exceptions, $path);
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      $path = $root . $path;
      if (is_dir($path)) {
        $this->_folder_redirect_list_all_files($path, $path_from_preg, $path_to, $root, $root_preg, $exceptions);
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Helper function to scan the dir and its sub-dir.
   */
  public function _folder_redirect_list_all_files($path, $path_from_preg, $path_to, $root, $root_preg, $exceptions, $different_path_from = '') {
    if (!isset($redirects)) {
      $redirects = [];
    }
    $files = array_diff(scandir($path), array_map('trim', $exceptions));
    foreach ($files as $file) {
      if (is_dir($path . "/{$file}")) {
        if (!empty($different_path_from)) {
          $this->_folder_redirect_list_all_files($path . "/{$file}", $path_from_preg, $path_to, $root, $root_preg, $exceptions, $different_path_from);
        }
        else {
          $this->_folder_redirect_list_all_files($path . "/{$file}", $path_from_preg, $path_to, $root, $root_preg, $exceptions);
        }
      }
      else {
        if (!empty($different_path_from)) {
          preg_match("/" . $root_preg . "(...+)/", $path . "/{$file}", $out);
          preg_match("/([a-zA-Z0-9-_]+)([\/])([.a-zA-Z0-9-_\/]+)/", $out[1], $out1);
          $redirect_from = $different_path_from . '/' . $out1[3];
          $redirect_to = $path_to . $out1[3];;
        }
        else {
          preg_match("/" . $root_preg . "(...+)/", $path . "/{$file}", $out);
          $redirect_from = $out[1];
          preg_match("/" . $path_from_preg . "\/(...+)/", $redirect_from, $out1);
          $redirect_to = $path_to . $out1[1];
        }
        $redirects[$redirect_from] = $redirect_to;
      }
    }
    $this->_folder_redirect_import_redirects($redirects);
  }

  /**
   * Helper function to import bulk redirects.
   */
  public function _folder_redirect_import_redirects($redirects) {
    foreach ($redirects as $from_url => $to_url) {
      if (!$this->redirectRepository->findBySourcePath(($from_url))) {
        Redirect::create([
          'redirect_source'   => $from_url,
          'redirect_redirect' => 'internal:/' . $to_url,
          'language'          => 'und',
          'status_code'       => '301',
        ])->save();
      }
      else {
        $this->messenger->addMessage($this->t('Redirect already exsists for path<i> "@path" </i>', ['@path' => $from_url]), 'warning', TRUE);
      }
    }
  }

}
