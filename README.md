--------------------------------------------------------------------------------
Folder Redirect
--------------------------------------------------------------------------------
Auto generate 301 redirects for the files within a folder, useful for folder
that contains large amount of files & are moved from one path to another.
path :  admin/config/search/redirect/folder_redirect

--------------------------------------------------------------------------------
Features
--------------------------------------------------------------------------------
Allows users, with right permission to generate redirects for all files within a
 folder providing path to redirect from & redirect to.
Allows users to scan a folder located anywhere under the root folder & add
 redirects of all of its containing files
 Users can add exceptions for files or folders, so the redirects won't be
 generated for that particular file or folder.

--------------------------------------------------------------------------------
Configurations
--------------------------------------------------------------------------------
Need to install and enable redirect

--------------------------------------------------------------------------------
Use Cases
--------------------------------------------------------------------------------
Generate redirects for files within folder
Useful if files are moved from one root path to another
